package br.com.ucsal.TedBd2jpajdbc.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.ucsal.TedBd2jpajdbc.model.ConsultaModel;
import br.com.ucsal.TedBd2jpajdbc.model.MedicoModel;
import br.com.ucsal.TedBd2jpajdbc.controller.ConsultaController;



/*
 * @Repository public class ConsultaRepository {
 * 
 * @PersistenceContext EntityManager em;
 * 
 * @Transactional public ConsultaModel insert(ConsultaModel cm) { try {
 * em.persist(cm);; }catch (Exception e) { System.out.println(e); } return cm; }
 * 
 * 
 * @Transactional public List<ConsultaModel> listar() {
 * 
 * List<ConsultaModel> lista = null; lista = em.createQuery("From" +
 * ConsultaModel.class).getResultList();
 * 
 * return lista;
 * 
 * }
 * 
 * @Transactional public ConsultaModel findById ( Integer id) { return
 * em.find(ConsultaModel.class, id); }
 * 
 * @Transactional public ConsultaModel edit (ConsultaModel cm) { em.merge(cm);
 * return cm; }
 * 
 * @Transactional(readOnly = false, propagation = Propagation.REQUIRED) public
 * void delete(Integer id) { ConsultaModel cm = em.find(ConsultaModel.class,
 * id);
 * 
 * try { em.remove(cm); }catch (Exception e) { System.out.println(e+"erro"); } }
 * }
 */
  
  @Repository public class ConsultaRepository {
  
  @Autowired private JdbcTemplate jdbcTemplate;
  
  @Transactional
  public int countConsulta() { return
  jdbcTemplate.queryForObject("select count(*) from consulta_model",
  Integer.class); }
  
  @Transactional
  public int saveConsulta(ConsultaModel consulta) { return jdbcTemplate.update(
  "insert into consulta_model (medico_id, paciente_id, hora_inicio) values(?,?,?)"
  , consulta.getMedico().getMedicoId(), consulta.getPaciente().getPacienteId(), consulta.getHoraInicio()); }
  
  @Transactional
  public int updateMedicoConsulta(ConsultaModel consulta) { return
  jdbcTemplate.update( "update consulta_model set medico_id = ? where id = ?",
  consulta.getMedico().getMedicoId(), consulta.getId()); }
  
  @Transactional
  public int deleteConsultaById(Integer id) { return jdbcTemplate.update(
  "delete consulta_model where id = ?", id); }
  
  @Transactional
  public List<ConsultaModel> findAll() { String sql =
  "select * FROM consulta_model"; RowMapper<ConsultaModel> rowMapper =
  (RowMapper<ConsultaModel>) new ConsultaModel(); return
  this.jdbcTemplate.query(sql, rowMapper); } }
 
 