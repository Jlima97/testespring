package br.com.ucsal.TedBd2jpajdbc.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.ucsal.TedBd2jpajdbc.model.MedicoModel;

@Repository
public class MedicoRepository {
	@PersistenceContext
	EntityManager em;

	@Transactional
	public MedicoModel insert(MedicoModel mm) {
		try {
			em.persist(mm);;
		}catch (Exception e) {
			System.out.println(e);
		}
		return mm;
	}


	@Transactional	
	public List<MedicoModel> listar() {

		List<MedicoModel> lista = null;
		lista = em.createQuery("From MedicoModel").getResultList();

		return lista;

	}

	@Transactional
	public MedicoModel findById ( Integer id) {	
		return em.find(MedicoModel.class, id);
	}

	@Transactional
	public MedicoModel edit (MedicoModel mm) {
		em.merge(mm);
		return mm;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(Integer id) {
		MedicoModel mm = em.find(MedicoModel.class, id);

		try {
			em.remove(mm);
		}catch (Exception e) {
			System.out.println(e+"erro");
		}
	}


}
