package br.com.ucsal.TedBd2jpajdbc.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.ucsal.TedBd2jpajdbc.model.MedicoModel;
import br.com.ucsal.TedBd2jpajdbc.model.Medico_Especialidade;

@Repository
public class Medico_EspRepository {
	@PersistenceContext
	EntityManager em;

	@Transactional
	public String insert(Medico_Especialidade mE) {
		try {
			em.persist(mE);;
		}catch (Exception e) {
			System.out.println(e);
		}
		return "add com sucesso";
	}


	@Transactional	
	public List<Medico_Especialidade> listar() {

		List<Medico_Especialidade> lista = null;
		lista = em.createQuery("From Medico_Especialidade").getResultList();

		return lista;

	}

	@Transactional
	public Medico_Especialidade findById ( Integer id) {	
		return em.find(Medico_Especialidade.class, id);
	}

	@Transactional
	public Medico_Especialidade edit (Medico_Especialidade mE) {
		em.merge(mE);
		return mE;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(Integer id) {
		Medico_Especialidade mE = em.find(Medico_Especialidade.class, id);

		try {
			em.remove(mE);
		}catch (Exception e) {
			System.out.println(e+"erro");
		}
	}


}
