package br.com.ucsal.TedBd2jpajdbc.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.TransactionScoped;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.ucsal.TedBd2jpajdbc.model.MedicoModel;
import br.com.ucsal.TedBd2jpajdbc.model.PacienteModel;

@Repository
public class PacienteRepository {

	@PersistenceContext
	EntityManager em;

	@Transactional
	public PacienteModel insert(PacienteModel pm) {
		try {
			em.persist(pm);;
		}catch (Exception e) {
			System.out.println(e);
		}
		return pm;
	}


	@Transactional	
	public List<PacienteModel> listar() {

		List<PacienteModel> lista = null;
		lista = em.createQuery("From PacienteModel").getResultList();

		return lista;

	}

	@Transactional
	public PacienteModel findById ( Integer id) {	
		return em.find(PacienteModel.class, id);
	}

	@Transactional
	public PacienteModel edit (PacienteModel pm) {
		em.merge(pm);
		return pm;
	}

	@Transactional(readOnly = false, propagation = Propagation.REQUIRED)
	public void delete(Integer id) {
		PacienteModel pm = em.find(PacienteModel.class, id);

		try {
			em.remove(pm);
		}catch (Exception e) {
			System.out.println(e+"erro");
		}
	}




}
