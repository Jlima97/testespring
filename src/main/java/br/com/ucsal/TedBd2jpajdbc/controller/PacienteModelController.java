package br.com.ucsal.TedBd2jpajdbc.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.ucsal.TedBd2jpajdbc.model.ConsultaModel;
import br.com.ucsal.TedBd2jpajdbc.model.MedicoModel;
import br.com.ucsal.TedBd2jpajdbc.model.Medico_Especialidade;
import br.com.ucsal.TedBd2jpajdbc.model.PacienteModel;
import br.com.ucsal.TedBd2jpajdbc.repository.ConsultaRepository;
import br.com.ucsal.TedBd2jpajdbc.repository.MedicoRepository;
import br.com.ucsal.TedBd2jpajdbc.repository.PacienteRepository;
@RestController
public class PacienteModelController {
	@Autowired
	private PacienteRepository _pacienteRepository;
	
	
	@RequestMapping(value = "/paciente", method =  RequestMethod.POST)
    public PacienteModel Post(@Validated @RequestBody PacienteModel pm){
		
			return _pacienteRepository.insert(pm);
		        
    }
	
	@RequestMapping(value = "/pacientes", method = RequestMethod.GET)
    public List<PacienteModel> Get() {
        return _pacienteRepository.listar();
    }

	@RequestMapping(value = "/paciente/{id}", method = RequestMethod.GET)
    public ResponseEntity<PacienteModel> GetById(@PathVariable(value = "id") Integer id)
    {
		PacienteModel pm = _pacienteRepository.findById(id);
        if(pm != null) {
            return ResponseEntity.ok(pm);

        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

	@RequestMapping(value = "/paciente/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<PacienteModel> Put(@PathVariable(value = "id") Integer id, @RequestBody PacienteModel newpm)
    {
        PacienteModel oldpm = _pacienteRepository.findById(id);
        if(oldpm!=null){
            
            oldpm.setNome(newpm.getNome());
            oldpm.setCpf(newpm.getCpf());
            oldpm.setRg(newpm.getRg());
            _pacienteRepository.edit(oldpm);
            return ResponseEntity.ok(newpm);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

	@RequestMapping(value = "/paciente/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") Integer id)
    {
        PacienteModel pm = _pacienteRepository.findById(id);
        if(pm!=null){
            _pacienteRepository.delete(pm.getPacienteId());
            return ResponseEntity.ok("Usuario Excluido");
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

	
	

}
