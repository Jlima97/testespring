package br.com.ucsal.TedBd2jpajdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ucsal.TedBd2jpajdbc.model.ConsultaModel;
import br.com.ucsal.TedBd2jpajdbc.model.MedicoModel;
import br.com.ucsal.TedBd2jpajdbc.model.PacienteModel;
import br.com.ucsal.TedBd2jpajdbc.repository.ConsultaRepository;
import br.com.ucsal.TedBd2jpajdbc.repository.MedicoRepository;
import br.com.ucsal.TedBd2jpajdbc.repository.PacienteRepository;

@RestController
public class ConsultaController {
	
	@Autowired
	private PacienteRepository _pacienteRepository;
	@Autowired
	private MedicoRepository _medicoRepository;
	@Autowired
	private ConsultaRepository _consultaRepository;
	
	
	@RequestMapping(value = "/consulta/pac/{id}/med/{idm}", method =  RequestMethod.POST)
    public String addEsp(@PathVariable(value = "id") Integer id, @PathVariable(value = "idm") Integer idm, @RequestBody ConsultaModel ce ){

		PacienteModel pm = _pacienteRepository.findById(id);
		MedicoModel md = _medicoRepository.findById(idm);
		ce.setPaciente(pm);
		ce.setMedico(md);
		_consultaRepository.saveConsulta(ce);
		//_consultaRepository.insert(ce);
		
		
			return"ok";    
    }
	

}
