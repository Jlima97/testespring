package br.com.ucsal.TedBd2jpajdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.ucsal.TedBd2jpajdbc.model.MedicoModel;
import br.com.ucsal.TedBd2jpajdbc.model.Medico_Especialidade;
import br.com.ucsal.TedBd2jpajdbc.repository.MedicoRepository;
import br.com.ucsal.TedBd2jpajdbc.repository.Medico_EspRepository;

@RestController
public class MedicoController {
	@Autowired
	private MedicoRepository _medicoRepository;
	
	@Autowired
	private Medico_EspRepository _medEsp;
	@RequestMapping(value = "/medico", method =  RequestMethod.POST)
    public MedicoModel Post(@Validated @RequestBody MedicoModel mm){
		
			return _medicoRepository.insert(mm);
		        
    }
	
	@RequestMapping(value = "/medicos", method = RequestMethod.GET)
    public List<MedicoModel> Get() {
        return _medicoRepository.listar();
    }

	@RequestMapping(value = "/medico/{id}", method = RequestMethod.GET)
    public ResponseEntity<MedicoModel> GetById(@PathVariable(value = "id") Integer id)
    {
		MedicoModel mm = _medicoRepository.findById(id);
        if(mm != null) {
            return ResponseEntity.ok(mm);

        } else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

	@RequestMapping(value = "/medico/{id}", method =  RequestMethod.PUT)
    public ResponseEntity<MedicoModel> Put(@PathVariable(value = "id") Integer id, @RequestBody MedicoModel newmm)
    {
        MedicoModel oldmm = _medicoRepository.findById(id);
        if(oldmm!=null){
            
            oldmm.setNome(newmm.getNome());
            oldmm.setCrm(newmm.getCrm());
            _medicoRepository.edit(oldmm);
            return ResponseEntity.ok(newmm);
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

	@RequestMapping(value = "/medico/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> Delete(@PathVariable(value = "id") Integer id)
    {
        MedicoModel mm = _medicoRepository.findById(id);
        if(mm!=null){
            _medicoRepository.delete(mm.getMedicoId());
            return ResponseEntity.ok("Usuario Excluido");
        }
        else
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }
	
	@RequestMapping(value = "/medico/{id}", method =  RequestMethod.POST)
    public String addEsp(@PathVariable(value = "id") Integer id,@RequestBody Medico_Especialidade mE ){
			MedicoModel md = _medicoRepository.findById(id);
			mE.setMedico(md);
			_medEsp.insert(mE);
			return "Especialidade add";
		        
    }
		
	
	
	
	}
	


