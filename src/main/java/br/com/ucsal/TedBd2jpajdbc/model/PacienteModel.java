package br.com.ucsal.TedBd2jpajdbc.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;


@Entity
public class PacienteModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int pacienteId;
	
	@NotNull
	private String nome;
	
	@NotNull
	private String cpf;
	
	@NotNull
	private String rg;
	@JsonIgnore
	@OneToMany(mappedBy = "paciente", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ConsultaModel> consultas = new ArrayList<ConsultaModel>();
	
	@ElementCollection
	@CollectionTable(name = "TELEFONE")
	private Set<String> telefones = new HashSet<>();
	
	public int getPacienteId() {
		return pacienteId;
	}
	public void setPacienteId(int pacienteId) {
		this.pacienteId = pacienteId;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getRg() {
		return rg;
	}
	public void setRg(String rg) {
		this.rg = rg;
	}

	
	public List<ConsultaModel> getConsultas() {
		return consultas;
	}
	public void setConsultas(List<ConsultaModel> consultas) {
		this.consultas = consultas;
	}
	
	
	
	
	
	public Set<String> getTelefones() {
		return telefones;
	}
	public void setTelefones(Set<String> telefones) {
		this.telefones = telefones;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + pacienteId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PacienteModel other = (PacienteModel) obj;
		if (pacienteId != other.pacienteId)
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "PacienteModel [pacienteId=" + pacienteId + ", nome=" + nome + ", cpf=" + cpf + ", rg=" + rg
				+ ", consultas=" + consultas + ", telefones=" + telefones + "]";
	}
	
	
	

}

