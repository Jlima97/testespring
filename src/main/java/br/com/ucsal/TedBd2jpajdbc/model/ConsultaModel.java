package br.com.ucsal.TedBd2jpajdbc.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class ConsultaModel implements Serializable{

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@ManyToOne(cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
	@JsonBackReference
	@JoinColumn(name = "medicoId")
	private MedicoModel medico;

	@ManyToOne
	@JoinColumn(name = "pacienteId")
	private PacienteModel paciente;
	
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private LocalDateTime horaInicio;
	

	public Integer getId() {
		return id;
	}

	public MedicoModel getMedico() {
		return medico;
	}

	public PacienteModel getPaciente() {
		return paciente;
	}

	
	
	public void setId(Integer id) {
		this.id = id;
	}

	public void setMedico(MedicoModel medico) {
		this.medico = medico;
	}

	public void setPaciente(PacienteModel paciente) {
		this.paciente = paciente;
	}


	

	public LocalDateTime getHoraInicio() {
		return horaInicio;
	}

	public void setHoraInicio(LocalDateTime horaInicio) {
		this.horaInicio = horaInicio;
	}

	
	

	@Override
	public String toString() {
		return "ConsultaModel [id=" + id + ", medico=" + medico + ", paciente=" + paciente + ", horaInicio="
				+ horaInicio + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ConsultaModel other = (ConsultaModel) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
