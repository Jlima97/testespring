package br.com.ucsal.TedBd2jpajdbc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sun.istack.NotNull;

@Entity
public class MedicoModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int medicoId;
	

	private String nome;
	@NotNull
	private String crm;

	@JsonManagedReference
	@OneToMany(mappedBy = "medico", cascade=CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Medico_Especialidade> especialidades = new ArrayList<Medico_Especialidade>();
	
	@JsonManagedReference
	@OneToMany(mappedBy = "medico", cascade=CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ConsultaModel> consultas = new ArrayList<ConsultaModel>();
	
	public int getMedicoId() {
		return medicoId;
	}
	public void setMedicoId(int medicoId) {
		this.medicoId = medicoId;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCrm() {
		return crm;
	}
	public void setCrm(String crm) {
		this.crm = crm;
	}
	public List<Medico_Especialidade> getEspecialidades() {
		return especialidades;
	}
	public List<ConsultaModel> getConsultas() {
		return consultas;
	}
	public void setEspecialidades(List<Medico_Especialidade> especialidades) {
		this.especialidades = especialidades;
	}
	public void setConsultas(List<ConsultaModel> consultas) {
		this.consultas = consultas;
	}
	@Override
	public String toString() {
		return "MedicoModel [medicoId=" + medicoId + ", nome=" + nome + ", crm=" + crm + ", especialidades="
				+ especialidades + ", consultas=" + consultas + "]";
	}
	
		



}
