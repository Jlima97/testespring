package br.com.ucsal.TedBd2jpajdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TedBd2JpaJdbcApplication {

	public static void main(String[] args) {
		SpringApplication.run(TedBd2JpaJdbcApplication.class, args);
	}

}
